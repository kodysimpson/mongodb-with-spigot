package me.kodysimpson.mongodb;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.function.Consumer;

public final class MongoDB extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic

        //connect to the server/database/collection
        MongoClient mongoClient = MongoClients.create("mongodb+srv://bob123:password!23@plugintest-w08nt.gcp.mongodb.net/test?retryWrites=true&w=majority");
        MongoDatabase database = mongoClient.getDatabase("sample_weatherdata");
        MongoCollection<Document> col = database.getCollection("data");

        //Print out the contents of each document in the collection
        col.find().forEach((Consumer<Document>) document -> {
            System.out.println(document.toJson());
        });

        //Write a new document to the collection
        Document data = new Document("type", "car")
                .append("wheels", 4)
                .append("isFast", true);
        col.insertOne(data);

        System.out.println("Plugin has started up");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
